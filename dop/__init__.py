import itertools
import subprocess

# http://stackoverflow.com/questions/287871/print-in-terminal-with-colors-using-python
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class Option:
    """
    Manage a single option for a dolfin run. Can be iterable.
    """

    def __init__(self, key, vals, iterate=True):
        if isinstance(key, str):
            key = [key]
        self.keys = key

        if isinstance(vals, (str, int, float)):
            vals = [vals]

        assert(len(vals) % len(self.keys) == 0)
        self.vals = vals
        self.iterate = len(vals) > 1 and iterate

    def str(self, idx):
        assert(idx < self.num_permutes())
        return " ".join(key + " " + str(self.vals[idx*len(self.keys) + j])
                           for j, key in enumerate(self.cmd_keys()))

    def key_value(self, idx):
        assert(idx < self.num_permutes())
        return dict((key, self.vals[idx*len(self.keys) + j]) for (j, key) in enumerate(self.get_key()))

    def get_key(self):
        return self.keys

    def num_permutes(self):
        return len(self.vals) // len(self.keys)

    def cmd_keys(self):
        return map(lambda s: "--" + s, self.keys)


class PETScOption(Option):
    """
    Same as Option but prefixes keys with --petsc.
    """

    def cmd_keys(self):
        return map(lambda s: "--petsc." + s, self.keys)


# class DependentOption(Option):
#
#     def __init__(self, key, option, f):
#         self.keys = [key]
#         assert isinstance(option, Option)
#         self.o_vals = map(f, option.vals)
#
#     def num_permutes(self):


class OptionCollection:
    """
    A collection of Options to be permuted and used in a run.
    """

    def __init__(self):
        self.opt = []

    def add(self, opt):
        self.opt.append(opt)

    def number_of_iterations(self):
        count = 1
        for opt in self.opt:
            if opt.iterate:
                count *= opt.num_permutes()
        return count

    def iterate_options(self):
        opt_perm_range = [list(range(o.num_permutes())) for o in self.opt]
        opt_permutes = list(itertools.product(*opt_perm_range))

        n_its = self.number_of_iterations()
        for it in range(n_its):
            yield OptionSet(self.opt, opt_permutes[it])

    def keys(self):
        return [o.get_key() for o in self.opt]

    def keys_flat(self):
        return [k for o in self.opt for k in o.get_key()]


class OptionSet:
    """
    A frozen configuration of options specified by the indices.
    I.e. options.get(indices[option_index])
    """

    def __init__(self, options, indices):
        self.indices = indices
        self.opt = options

    def str(self):
        return " ".join([o.str(self.indices[o_idx]) for o_idx, o in enumerate(self.opt)])

    def key_value(self):
        kv = {}
        for o_idx, o in enumerate(self.opt):
            kv.update(o.key_value(self.indices[o_idx]))
        return kv

    def report(self):
        return self.key_value()

    def __repr__(self):
        return self.str()


class BatchJobReport:
    """
    Data collection from a job
    """

    def __init__(self, report_dict):
        self.rep = report_dict

    def generate_table(self):
        return [row for row in zip(*([key] + value
                                     for key, value in sorted(self.rep.items())))]

    def __repr__(self):
        table = self.generate_table()
        msg = ""
        for row in table:
            msg += ",\t".join(row) + "\n"
        return msg

    def csv(self):
        table = self.generate_table()
        msg = ""
        for row in table:
            msg += ",".join(row) + "\n"
        return msg

    def table(self):
        import numpy as np
        import pandas as pd
        rows = self.generate_table()
        header = rows[0]
        data = np.array(rows[1:])
        frame = pd.DataFrame(data, columns=header)
        return frame



class BatchJob:
    """
    Schedule a batch of jobs to iterate over an option set.
    """

    def __init__(self, fname, np, option_collection, timeout=120, python=True):
        assert(isinstance(option_collection, OptionCollection))
        self.opt_col = option_collection
        if not isinstance(np, (list, tuple)):
            np = [np]
        self.np = np
        self.fname = fname
        self.measures = []
        self.timeout = timeout
        self.python = "python" if python else ""

    def measure_timing(self, name):
        self.measure_output_line(name, 2)

    def measure_output_line(self, name, idx_after_split=0):
        self.measures.append((name, idx_after_split))

    def run(self):
        failed_jobs = []

        report = {}
        for k in self.opt_col.keys_flat():
            report[k] = []
        for k, idx in self.measures:
            report[k] = []
        report["np"] = []

        for oset in self.opt_col.iterate_options():
            for np in self.np:
                report["np"].append(str(np))
                exe = ("mpirun -np %d python3 %s " if self.python else "mpirun -np %d %s ") % (np, self.fname)
                exe += oset.str()

                print("Running job: " + exe)

                try:
                    out = subprocess.check_output(exe.split(" "))#, timeout=self.timeout)
                    out = out.decode("utf-8")
                except subprocess.CalledProcessError as e:
                    failed_jobs.append(exe)
                    print(bcolors.FAIL + ("Job failed: %s\n %s" % (exe, e.output)) + bcolors.ENDC)
                    out = "Fail"

                oreport = oset.report()
                for k, v in oreport.items():
                    report[k].append(v)

                if out == "Fail": # Run failed
                    for name, idx in self.measures:
                        report[name].append(float('nan'))
                else: # Run succeeded, measure timings
                    for line in out.splitlines():
                        line = line.replace("Process 0: ", "")
                        for name, idx in self.measures:
                            if line.startswith(name):
                                splitz = line.replace(name, "").split()
                                report[name].append(splitz[idx])

        if len(failed_jobs) > 0:
            print(bcolors.WARNING + "Failed jobs:\n " + "\n".join(failed_jobs) + bcolors.ENDC)
        else:
            print(bcolors.OKGREEN + "All jobs successful." + bcolors.ENDC)
        return BatchJobReport(report)