from petsc4py import PETSc
from dolfin import *

t_total = Timer("TIME_total")

parameters.add("neles", 16)
parameters.add("mass_precond", True)
parameters.parse()

parameters["std_out_all_processes"] = False
neles = parameters["neles"]

mesh = UnitSquareMesh(neles, neles)
n = FacetNormal(mesh)


# Define function spaces
P2 = VectorElement("Lagrange", mesh.ufl_cell(), 2)
P1 = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
TH = P2 * P1
W = FunctionSpace(mesh, TH)
info("DoFs " + str(W.dim()))

u_soln = Expression(("-(x[1]*cos(x[1]) + sin(x[1]))*exp(x[0])",
                     "x[1]*sin(x[1])*exp(x[0])"), degree=4, domain=mesh)
p_soln = Expression("2.0*exp(x[0])*sin(x[1]) + 1.5797803888225995912/3.0", degree=4)

gN = dot(grad(u_soln) - p_soln*Identity(2), n)

ff = MeshFunction("size_t", mesh, 1, 0)
CompiledSubDomain("on_boundary").mark(ff, 1)
CompiledSubDomain("near(x[0], 1.0) and on_boundary").mark(ff, 2)
ds = Measure("ds", subdomain_data=ff)

bcs = [DirichletBC(W.sub(0), u_soln, ff, 1)]

# Define variational problem
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)
f = Constant((0, 0))
a = (inner(grad(u), grad(v)) - div(v)*p - q*div(u))*dx
L = inner(f, v)*dx + dot(gN, v)*ds(2)

solver = PETScKrylovSolver()
solver.set_from_options()
A, b = assemble_system(a, L, bcs)

if parameters["mass_precond"] and not solver.ksp().getPC().getType() == "lu":
    P_form = inner(grad(u), grad(v))*dx + p*q*dx
    P, _ = assemble_system(P_form, L, bcs)
    solver.set_operators(A, P)
else:
    solver.set_operator(A)

if solver.ksp().getPC().getType() == "fieldsplit":
    ksp = solver.ksp()
    pc = ksp.getPC()
    pc.setType(PETSc.PC.Type.FIELDSPLIT)
    is0 = PETSc.IS().createGeneral(W.sub(0).dofmap().dofs())
    is1 = PETSc.IS().createGeneral(W.sub(1).dofmap().dofs())
    pc.setFieldSplitIS(('u', is0), ('p', is1))

w = Function(W)

t_solve = Timer("TIME_solve")
solver.solve(w.vector(), b)
t_solve.stop()

# # Split the mixed solution using a shallow copy
(uh, ph) = w.split()

l2error = errornorm(u_soln, uh, "l2")

t_total.stop()

list_timings(TimingClear.keep, [TimingType.wall])