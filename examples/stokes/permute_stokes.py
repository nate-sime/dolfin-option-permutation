from dop import *

np = [1, 2, 3, 4]

problem_options = OptionCollection()

# User options
problem_options.add(Option("neles", 8))

# PETSc options
problem_options.add(PETScOption("ksp_rtol", 1e-10))
problem_options.add(PETScOption(
    ("ksp_type", "pc_type", "pc_factor_mat_solver_type"),
    ("pre_only",      "lu",                     "mumps",
        "tfqmr",   "hypre",                          "",
        "gmres",   "hypre",                          "")
))

bj = BatchJob("stokes.py", np, problem_options)
bj.measure_timing("TIME_total")
bj.measure_output_line("DoFs")
rep = bj.run()

import pandas as pd
pd.set_option("display.width", 200)
df = rep.table()
df.to_csv("stokes_solve_data.csv")
