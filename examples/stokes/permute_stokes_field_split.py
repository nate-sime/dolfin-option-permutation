from dop import *

np = [1, 2, 3, 4]

problem_options = OptionCollection()

# User options
problem_options.add(Option("neles", 64))

# PETSc options
problem_options.add(PETScOption(
    ("ksp_type", "ksp_gmres_restart", "ksp_gmres_classicalgramschmidt", "ksp_rtol",    "pc_type", "pc_fieldsplit_type", "pc_fieldsplit_schur_fact_type", "fieldsplit_u_ksp_type", "fieldsplit_u_pc_type", "fieldsplit_p_ksp_type", "fieldsplit_p_pc_type"),
    (   "tfqmr",                  30,                             True,      1e-10, "fieldsplit",              "schur",                         "lower",               "preonly",                   "lu",               "preonly",                 "none",
       "fgmres",                  30,                             True,      1e-10, "fieldsplit",     "multiplicative",                         "upper",               "preonly",                "hypre",               "preonly",               "jacobi",
       "fgmres",                  30,                             True,      1e-10, "fieldsplit",              "schur",                          "diag",               "preonly",                 "gamg",                "minres",                 "none",
       "fgmres",                  30,                             True,      1e-10, "fieldsplit",              "schur",                         "lower",               "preonly",                 "gamg",                "minres",                 "none",
       "fgmres",                  30,                             True,      1e-10, "fieldsplit",              "schur",                         "upper",               "preonly",                 "gamg",                "minres",                 "none",
       "fgmres",                  30,                             True,      1e-10, "fieldsplit",              "schur",                         "lower",               "preonly",                 "gamg",                "minres",                  "lsc",
       "fgmres",                  30,                             True,      1e-10, "fieldsplit",              "schur",                          "full",               "preonly",                 "gamg",                "minres",                 "lsc")
))

bj = BatchJob("stokes.py", np, problem_options)
bj.measure_timing("TIME_total")
bj.measure_output_line("DoFs")
rep = bj.run()

import pandas as pd
pd.set_option("display.width", 200)
df = rep.table()
df.to_csv("stokes_solve_data.csv")
