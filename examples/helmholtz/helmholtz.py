
__author__ = 'njcs4'

from dolfin import *

t_total = Timer("TIME_total")

parameters.add("eps-type", "krylov-schur")
parameters.add("neles", 16)
parameters.parse()
parameters["std_out_all_processes"] = False

eps_type = parameters["eps-type"]
neles = parameters["neles"]

mesh = UnitCubeMesh(neles, neles, neles)

V = FunctionSpace(mesh, 'CG', 1)
info("DoFs " + str(V.dim()))

u, v =  TrialFunction(V), TestFunction(V)
a = dot(grad(u), grad(v))*dx
m = u*v*dx
L_dummy = Constant(0.0)*v*dx

# Assemble the stiffness matrix (S) and mass matrix (T)
K = PETScMatrix()
M = PETScMatrix()

x0 = PETScVector()

bcs = []

t_ass = Timer("TIME_assembly")
assemble_system(a, L_dummy, bcs, A_tensor=K, b_tensor=x0)
assemble_system(m, L_dummy, bcs, A_tensor=M, b_tensor=x0)
t_ass.stop()

u0 = Function(V)

esolver = SLEPcEigenSolver(K, M)

esolver.parameters['problem_type'] = 'gen_hermitian'
esolver.parameters['spectrum'] = 'smallest real'

if eps_type == 'jacobi-davidson':
   esolver.parameters["solver"] = "jacobi-davidson"
   nullspace_basis = as_backend_type(u0.vector().copy())
   V.dofmap().set(nullspace_basis, 1.0)
   esolver.set_deflation_space(nullspace_basis)
else:
   esolver.parameters["solver"] = eps_type
   esolver.parameters["spectral_transform"] = 'shift-and-invert'
   esolver.parameters['spectral_shift'] = 10.0

nev = 20
t_solve = Timer("TIME_solve")
esolver.solve(nev)
t_solve.stop()

t_get = Timer("TIME_get")
for i in range(nev):
   (lr, lc, r, c) = esolver.get_eigenpair(i)
t_get.stop()

t_total.stop()

list_timings(TimingClear_keep, [TimingType_wall])
