from dop import *

options = OptionCollection()

# PETScOptions
options.add(PETScOption("eps_max_it", 400))

# User options
options.add(Option("eps-type", ['jacobi-davidson', 'krylov-schur']))
dofs = [1e3, 5e3]#, 1e4, 5e4, 1e5, 5e5]
neles = list(map(lambda dofs: int(dofs**(1./3.) - 1), dofs))
options.add(Option("neles", neles))

# Numer of processes
np = [4]

bj = BatchJob("elastodynamics.py", np, options)
bj.measure_timing("TIME_total")
bj.measure_output_line("DoFs")
rep = bj.run()

import pandas as pd
pd.set_option("display.width", 200)
df = rep.table()
df.to_csv("elastodynamics_solve_data.csv")
