
__author__ = 'njcs4'

from dolfin import *

t_total = Timer("TIME_total")

def build_nullspace(V, x):
    """Function to build null space for 3D elasticity"""

    # Create list of vectors for null space
    nullspace_basis = [x.copy() for i in range(6)]

    # Build translational null space basis
    V.sub(0).dofmap().set(nullspace_basis[0], 1.0)
    V.sub(1).dofmap().set(nullspace_basis[1], 1.0)
    V.sub(2).dofmap().set(nullspace_basis[2], 1.0)

    # Build rotational null space basis
    V.sub(0).set_x(nullspace_basis[3], -1.0, 1)
    V.sub(1).set_x(nullspace_basis[3],  1.0, 0)
    V.sub(0).set_x(nullspace_basis[4],  1.0, 2)
    V.sub(2).set_x(nullspace_basis[4], -1.0, 0)
    V.sub(2).set_x(nullspace_basis[5],  1.0, 1)
    V.sub(1).set_x(nullspace_basis[5], -1.0, 2)

    for x in nullspace_basis:
        x.apply("insert")

    # Create vector space basis and orthogonalize
    basis = VectorSpaceBasis(nullspace_basis)
    basis.orthonormalize()

    return basis

parameters.add("eps-type", "krylov-schur")
parameters.add("neles", 16)
parameters.parse()
parameters["std_out_all_processes"] = False

eps_type = parameters["eps-type"]
neles = parameters["neles"]

mesh = UnitCubeMesh(neles, neles, neles)

V = VectorFunctionSpace(mesh, 'CG', 1)
u, v = TrialFunction(V), TestFunction(V)
info("DoFs " + str(V.dim()))

rho = Constant(2700.0)
E = 70e9
nu = 0.32

mu = Constant(E/(2.0*(1.0 + nu)))
lam = Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)))

a = inner(2*mu*sym(grad(u)) + lam*tr(sym(grad(u)))*Identity(mesh.geometry().dim()), sym(grad(v)))*dx
m = dot(rho*u, v)*dx
L_dummy = dot(Constant([0.0]*mesh.geometry().dim()), v)*dx

# Assemble the stiffness matrix (S) and mass matrix (T)
K = PETScMatrix()
M = PETScMatrix()

x0 = PETScVector()

bcs = []

assemble_system(a, L_dummy, bcs, A_tensor=K, b_tensor=x0)
assemble_system(m, L_dummy, bcs, A_tensor=M, b_tensor=x0)

u0 = Function(V)

esolver = SLEPcEigenSolver(K, M)

PETScOptions.set("eps_monitor")

esolver.parameters['problem_type'] = 'gen_hermitian'
esolver.parameters['spectrum'] = 'smallest real'

if eps_type == "krylov-schur":
    esolver.parameters["solver"] = "krylov-schur"
    esolver.parameters["spectral_transform"] = 'shift-and-invert'
    esolver.parameters['spectral_shift'] = 10.0

    PETScOptions.set("st_ksp_type", "preonly")
    PETScOptions.set("st_pc_type", "lu")
    PETScOptions.set("st_pc_factor_mat_solver_package", "mumps")
else:
    esolver.parameters["solver"] = "jacobi-davidson"

    nullspace_basis = build_nullspace(V, as_backend_type(u0.vector()))
    esolver.set_deflation_space(nullspace_basis)

    K.set_near_nullspace(nullspace_basis)

    PETScOptions.set("eps_jd_blocksize", 3)
    PETScOptions.set("eps_gd_double_expansion")

    PETScOptions.set("st_type", "precond")

    PETScOptions.set("eps_target", 1e6)
    PETScOptions.set("eps_target_real")

    # PETScOptions.set("st_ksp_type", "bcgsl")
    # PETScOptions.set("st_pc_type", "bjacobi")

    PETScOptions.set("st_ksp_type", "cg")
    PETScOptions.set("st_pc_type", "gamg")
    # PETScOptions.set("st_ksp_monitor_true_residual")

    PETScOptions.set("st_pc_gamg_type", "agg")
    PETScOptions.set("st_pc_gamg_agg_nsmooths", 1)
    PETScOptions.set("st_ksp_rtol", 1e-3)
    # # PETScOptions.set("st_ksp_atol", 1e-12)

    PETScOptions.set("st_pc_gamg_threshold", 0.02)
    # PETScOptions.set("pc_gamg_repartition", True)
    PETScOptions.set("st_pc_gamg_coarse_eq_limit", 1000)
    PETScOptions.set("st_pc_gamg_square_graph", 1)
    PETScOptions.set("st_pc_gamg_reuse_interpolation", True)

    PETScOptions.set("st_mg_levels_esteig_ksp_type", "cg")
    PETScOptions.set("st_mg_levels_ksp_type", "chebyshev")
    PETScOptions.set("st_mg_levels_pc_type", "jacobi")

    # PETScOptions.set("st_mg_levels_ksp_chebyshev_esteig_steps", 50)
    # PETScOptions.set("st_mg_levels_esteig_ksp_max_it", 50)
    PETScOptions.set("st_mg_levels_ksp_chebyshev_esteig_random")
    #
    # # PETScOptions.set("mg_coarse_sub_ksp_type", "preonly")
    # # PETScOptions.set("mg_coarse_sub_pc_type", "lu")
    # # PETScOptions.set("mg_coarse_sub_pc_factor_mat_solver_package", "umfpack")
    #
    # # PETScOptions.set("mg_levels_ksp_max_it", 1)


nev = 10
t_solve = Timer("TIME_solve")
esolver.solve(nev)
t_solve.stop()

t_get = Timer("TIME_get")
for i in range(nev):
   (lr, lc, r, c) = esolver.get_eigenpair(i)
t_get.stop()

t_total.stop()

list_timings(TimingClear_keep, [TimingType_wall])