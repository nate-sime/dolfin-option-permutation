
__author__ = 'njcs4'

from dolfin import *

t_total = Timer("TIME_total")

parameters.add("neles", 16)
parameters.parse()

parameters["std_out_all_processes"] = False
neles = parameters["neles"]

mesh = UnitCubeMesh(neles, neles, neles)

V = FunctionSpace(mesh, 'CG', 1)
info("DoFs " + str(V.dim()))

u, v =  TrialFunction(V), TestFunction(V)
a = dot(grad(u), grad(v))*dx
L = Constant(1.0)*v*dx

bcs = [DirichletBC(V, Constant(0.0), 'on_boundary')]

u0 = Function(V)

t_solve = Timer("TIME_solve")
solve(a == L, u0, bcs)
t_solve.stop()

t_total.stop()

list_timings(TimingClear.keep, [TimingType.wall])
