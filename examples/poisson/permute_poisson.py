from dop import *

np = [1, 2, 3, 4]

problem_options = OptionCollection()

# User options
problem_options.add(Option("neles", 32))

# PETSc options
problem_options.add(PETScOption("ksp_type", "preonly"))
problem_options.add(PETScOption("pc_type", "lu"))
problem_options.add(PETScOption("pc_factor_mat_solver_package", ["superlu_dist", "mumps"]))

bj = BatchJob("poisson.py", np, problem_options)
bj.measure_timing("TIME_total")
bj.measure_output_line("DoFs")
rep = bj.run()

import pandas as pd
pd.set_option("display.width", 200)
df = rep.table()
df.to_csv("poisson_solve_data.csv")
