import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

pd.set_option("display.width", 200)

# load and clean data
df = pd.read_csv('poisson_solve_data.csv')
df = df[df['TIME_total'].notnull()]

# Plot scatter for multiple hues
groups = df.groupby(['DoFs', 'ksp_type', 'pc_type', 'pc_factor_mat_solver_package'])
for name, group in groups:
    plt.plot(group['np'], group['TIME_total'], marker='o', linestyle='-', label=name)
plt.legend()
plt.xlabel("processes")
plt.ylabel("time (s)")
plt.show()