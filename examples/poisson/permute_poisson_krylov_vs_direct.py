from dop import *

np = [1, 2, 3, 4]

problem_options = OptionCollection()

# User options
problem_options.add(Option("neles", 32))

# PETSc options
problem_options.add(PETScOption(
    ("ksp_type", "pc_type", "pc_factor_mat_solver_package"),
        ("preonly", "lu",         "superlu_dist",
         "preonly", "lu",         "mumps",
         "cg",      "hypre",      "superlu_dist",
         "cg",      "gamg",       "superlu_dist",
         "gmres",   "hypre",      "superlu_dist",
         "gmres",   "gamg",       "superlu_dist"))
)

bj = BatchJob("poisson.py", np, problem_options)
bj.measure_timing("TIME_total")
bj.measure_output_line("DoFs")
rep = bj.run()

import pandas as pd
pd.set_option("display.width", 200)
df = rep.table()
df.to_csv("poisson_solve_data.csv")
